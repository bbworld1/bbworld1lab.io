---
title: "bbworld1"
description: human &middot; occasional code monkey
template: "home.html"
links:
    - name: gitlab
      link: https://gitlab.com/bbworld1/
    - name: github
      link: https://github.com/bbworld1/
    - name: medium
      link: https://vwangsf.medium.com/
    - name: vwangsf@gmail.com
      link: mailto:vwangsf@gmail.com
    - name: bbworld#6258
      link: https://discord.com/
    - name: bibify
      link: https://bibify.org/
    - name: yasg
      link: https://gitlab.com/bbworld1/yasg
    - name: kronoboard
      link: https://kronoboard.netlify.app/

display: Hey. Welcome to my website.
lead: bbworld1 &middot; human &middot; occasional code monkey
---
